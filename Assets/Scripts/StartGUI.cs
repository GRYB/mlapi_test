﻿using MLAPI;
using MLAPI.Transports.UNET;
using System;
using UnityEngine;

public class StartGUI : MonoBehaviour
{
    //IPv4 Address for local testing 
    [SerializeField] string serverAddress;
    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 300, 300));
        if (!NetworkingManager.Singleton.IsClient && !NetworkingManager.Singleton.IsHost)
        {
            StartButtons();
        }
        else {
            StatusLabel();
        }
        GUILayout.EndArea();

    }

    private void StatusLabel()
    {
        var mode = NetworkingManager.Singleton.IsHost ?
    "Host" : NetworkingManager.Singleton.IsServer ? "Server" : "Client";

        GUILayout.Label("Transport: " +
            NetworkingManager.Singleton.NetworkConfig.NetworkTransport.GetType().Name);
        GUILayout.Label("Mode: " + mode);
    }
    string ipText;
    void StartButtons()
    {
        if (GUILayout.Button("Host"))
            NetworkingManager.Singleton.StartHost();
        if (GUILayout.Button("Client-to localhost"))
        {
            ConnectClient("127.0.0.1");
        }
        if (GUILayout.Button("Client-to server"))
        {
            //IPv4 Address for local testing 
            // ConnectClient("1*2.1**.1.*");
            ConnectClient("127.0.0.1");

        }
        if (GUILayout.Button("Server"))
            NetworkingManager.Singleton.StartServer();
    }

    void ConnectClient(string address)
    {
        NetworkingManager.Singleton.GetComponent<UnetTransport>().ConnectAddress = address;
        NetworkingManager.Singleton.StartClient();
    }
}
