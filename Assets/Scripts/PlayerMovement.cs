﻿using MLAPI;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : NetworkedBehaviour
{
    [SerializeField] float speed = 5f;
    [SerializeField] Camera camera;
    [SerializeField] Transform moveTransform;

    CharacterController cc;

    private void Start()
    {
        cc = GetComponent<CharacterController>();
    }
    private void Update()
    {
        if (IsLocalPlayer)
        {
            MovePlayer();
            Look();
        }
    }

    void Look()
    {
        moveTransform.Rotate( new Vector3(0, Input.GetAxis("Mouse X") * 3f, 0));
        var cameraRotate = (Mathf.Clamp(Input.GetAxis("Mouse Y"), -45, 45) * 3f);
        camera.transform.localRotation = Quaternion.Euler(cameraRotate, 0, 0); 
    }
    void MovePlayer()
    {
        Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0 , Input.GetAxis("Vertical"));
        direction = Vector3.ClampMagnitude(direction, 1f);
        direction = moveTransform.TransformDirection(direction);
        cc.SimpleMove(direction * speed);
    }
}
